Fonctionnalité: Système de tag
    Contexte:
        Dans le but de rechercher des articles concernant
            une catégorie particulière
        En tant qu'utilisateur
        Je veux qu'en cliquant sur des mots-clés, tous les articles
            ou un échantillon de ceux-ci me soient présentés
