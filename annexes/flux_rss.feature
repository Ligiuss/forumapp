Fonctionnalité: Flux RSS
    Contexte:
        Dans le but d'obtenir des nouvelles du site rapidement
            autre qu'en se rendant manuellement dessus
        En tant qu'utilisateur
        Je désire pouvoir m'abonner à un flux RSS qui m'informera
            de toutes les nouveautés du site
