Fonctionnalité: Barre de recherche
    Contexte:
        Dans le but de rechercher des articles sur le site
        En tant qu'utilisateur
        Je souhaite pouvoir rechercher en entrant ma propre recherche personnalisée
    Scénario: Recherche d'un article du blog
        Etant donné que Je recherche sur https://blog.be un article sur java
        Quand J'entre 'Java' dans ['Recherche']
        Et que je clique sur ["#soumettre"]
        Alors je vois que l'url du naviguateur contient 'action=search&term=Java'
        Alors je vois que le titre de la page contient "Résultats pour 'Java'"
        Et que une liste contenant les différents posts en relation avec java s'affiche

    Scénario: Recherche d'un article du blog mais aucun article ne correspond à cette requête
        Etant donné que Je recherche sur https://blog.be un article sur oz
        Quand J'entre 'oz' dans ['Recherche']
        Et que je clique sur ["#soumettre"]
        Alors je vois que l'url du naviguateur contient 'action=search&term=oz'
        Alors je vois que le titre de la page contient "Aucun résultat pour le terme Oz, peut-être l'avez vous mal orthographié ?"
        Et que au centre du site web une image de chaton triste

    Scénario: Recherche d'un article du blog mais énormément d'articles correspondent à cette requête
        Etant donné que Je recherche sur https://blog.be un article sur python
        Quand J'entre 'python' dans ['Recherche']
        Et que je clique sur ["#soumettre"]
        Alors je vois que l'url du naviguateur contient 'action=search&term=python'
        Alors je vois que le titre de la page contient "Résultats pour 'python'"
        Et que une liste contenant les différents posts en relation avec java s'affiche
        Et que une pagination en dessous de la liste contenant [1..2..3] permettant d'accéder à la deuxième ou troisième page de résultat
