Fonctionnalité: Ajouter un article de blog facilement
    Contexte:
        Dans le but d'ajouter facilement des articles de blogs sans avoir à mettre le code à jour
        En tant qu'administrateur du site
        Je veux un outil pour ajouter des articles en ligne via une page spécialisée
