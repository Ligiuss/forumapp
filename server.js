/*
 *  Main server
 */
const express = require('express');
const PORT = 3000;

function start(router) {
    const app = express();
    let db = require("./database");
    db.connectToDataBase();

    var bodyParser = require('body-parser')
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: true}))
    //app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'ejs');

    app.use(express.static('public'));

    app.use("/", router);

    app.use("*", function(req, res) {
        res.render('page404', function(err,html) {
            if(err) next(err);
            res.status(404).send(html);
        });
    });

    // Gestion des erreurs
    app.use(function (err, req, res, next) {
        console.log(err);
        console.log(err.stack);
        res.status(500).send(err.stack);
    });

    let server = app.listen(PORT, function() {
        let host = server.address().address
        let port = server.address().port
        console.log(`App listening at http://%s:%s`, host, port);
    });


}

exports.start = start;
