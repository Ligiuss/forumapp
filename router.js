/*
 *  Manage the different routes of the website
 */
const express = require('express');
const path = require('path');
let fs = require('fs');

const router = express.Router();
let db = require("./database");

router.get("/", (req, res, next) => {
    let database = db.getDB();
    database.collection('Post').find().toArray((err, result) => {
    if (err) return console.log(err)
    res.render('index.ejs', {posts: result})
  })
});

router.get("/apropos", (req, res, next) => {
    res.render('about', function(err,html) {
        if(err) next(err);
        res.status(200).send(html);
    });
});

router.get("/nodejs", (req, res, next) => {
    res.render('nodejs', function(err,html) {
        if(err) next(err);
        res.status(200).send(html);
    });
});

router.post('/addpost', (req, res) => {
    let database = db.getDB();
    database.collection('Post').insertOne(req.body).then(res.redirect('/'))
});

router.post('/read_more', (req, res, next) => {
    var body = req.body.read_more_body
    handleRequest(body.toLowerCase(), {body: body}, res, next)
});

function handleRequest(file, options, res, next) {
    if (fs.existsSync("./views/"+file+".ejs")) {
        renderPage(file, options, res)
    } else {
        renderErrorPage(res)
    }
}

function renderPage(file, options, res) {
    res.render(file, options, (err, html) => {
                console.log(err)
                if (err) next(err)
                    res.status(200).send(html);
            })
}

function renderErrorPage(res) {
    res.render('page404', function(err,html) {
            if(err) next(err);
            res.status(404).send(html);
        });
}



module.exports = router
