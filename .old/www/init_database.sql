-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Mer 05 Juillet 2017 à 22:12
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `forum`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `Cat_id` int(11) NOT NULL,
  `Cat_titre` text NOT NULL,
  `Cat_description` text NOT NULL,
  `Cat_fk_categories` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`Cat_id`, `Cat_titre`, `Cat_description`, `Cat_fk_categories`) VALUES
(1, 'JAVA', 'Ensemble des Catégories et Sujets traitants de Java', NULL),
(2, 'PHP', 'Ensemble des Categories et Sujets traitant de PHP', NULL),
(3, 'Java EE', 'Sujet et categories traitant de Java EE', 1),
(4, 'Java SE', 'Sujets et catégories traitants de java SE', 1),
(5, 'Java Util', 'java utils', 3),
(6, 'PHP 1', 'Le vieux php !', 2),
(7, 'Python', 'Categories et sujets traitants de python', NULL),
(8, 'SQL', 'Catégories et Sujets traitants de SQL', NULL),
(9, 'PHP2', 'Catégories et Sujets traitants de PHP2', 2),
(10, 'PYTHON1', 'Catégories et Sujets traitants de Python1', 7),
(11, 'Python2', 'Catégories et Sujets traitants de Python2', 7),
(12, 'SQL2', 'Catégories et Sujets traitants de SQL2', 8),
(13, 'No-SQL', 'Catégories et Sujets traitants de No-SQL', 8),
(14, 'My SQL', 'Catégories et Sujets traitants de My SQL', 12);

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `Mess_id` int(11) NOT NULL,
  `Mess_FK_Uti_id` int(11) NOT NULL,
  `Mess_FK_Suj_id` int(11) NOT NULL,
  `Mess_date` timestamp NOT NULL,
  `Mess_contenu` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `messages`
--

INSERT INTO `messages` (`Mess_id`, `Mess_FK_Uti_id`, `Mess_FK_Suj_id`, `Mess_date`, `Mess_contenu`) VALUES
(1, 1, 1, '2017-07-05 18:55:56', 'Java c\'est super cool !'),
(2, 1, 2, '2017-07-05 20:08:29', 'Trop nuuuuul!'),
(3, 2, 3, '2017-07-05 21:46:25', 'oh oh !'),
(4, 3, 3, '2017-07-05 21:58:10', 'What ?'),
(5, 1, 3, '2017-07-05 22:05:08', ':/ ?');

-- --------------------------------------------------------

--
-- Structure de la table `sujets`
--

CREATE TABLE `sujets` (
  `Suj_id` int(11) NOT NULL,
  `Suj_titre` text NOT NULL,
  `Suj_description` text NOT NULL,
  `Suj_FK_Cat_id` bigint(20) NOT NULL,
  `Suj_FK_Uti_id` int(11) NOT NULL,
  `Suj_date` timestamp NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `sujets`
--

INSERT INTO `sujets` (`Suj_id`, `Suj_titre`, `Suj_description`, `Suj_FK_Cat_id`, `Suj_FK_Uti_id`, `Suj_date`) VALUES
(1, 'Java c\'est bien !', 'Waoouuuwwww', 1, 1, '2017-07-05 18:50:09'),
(2, 'Php c\'est pas super', 'pas super du tout', 2, 1, '2017-07-05 18:58:19'),
(3, 'Sujet Java SE', 'Bla bla bla', 3, 3, '2017-07-05 21:45:24');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `Uti_id` int(11) NOT NULL,
  `Uti_nom` varchar(50) NOT NULL,
  `Uti_prenom` varchar(50) NOT NULL,
  `Uti_mail` varchar(50) NOT NULL,
  `Uti_pseudo` varchar(30) NOT NULL,
  `Uti_admin` tinyint(1) DEFAULT NULL,
  `Uti_pwd` varchar(15) DEFAULT NULL,
  `Uti_datecreation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`Uti_id`, `Uti_nom`, `Uti_prenom`, `Uti_mail`, `Uti_pseudo`, `Uti_admin`, `Uti_pwd`, `Uti_datecreation`) VALUES
(1, 'LEONARD', 'Benjamin', 'benja.leonard.bl@gmail.com', 'Zead', 1, 'testforum', '2017-07-05 18:22:11'),
(2, 'BAPS', 'Fabien', 'Baps.fabien@gmail.com', 'Bapsfan', NULL, '12345', '2017-07-05 21:30:52'),
(3, 'SIQUET', 'Dylan', 'Dylan.siquet@gmail.com', 'Siq', NULL, '12345', '2017-07-05 21:32:39');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`Cat_id`),
  ADD KEY `Cat_FK_Cat_id` (`Cat_fk_categories`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`Mess_id`);

--
-- Index pour la table `sujets`
--
ALTER TABLE `sujets`
  ADD PRIMARY KEY (`Suj_id`),
  ADD UNIQUE KEY `Suj_FK_Cat_id` (`Suj_FK_Cat_id`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`Uti_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `Cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `Mess_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `sujets`
--
ALTER TABLE `sujets`
  MODIFY `Suj_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `Uti_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
