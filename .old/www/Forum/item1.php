<div class="flex-item item1">
    <?php
    //Nombre messages et dernière date
    $r = $db->query('SELECT count(*) as nbtot, MAX(Mess_date) as dernier, MAX(Mess_id) 
                    FROM messages');
    $Trucsdivers= $r->fetchAll();
    //Dernier posteur et contenu du message
    $r = $db->query('SELECT mess_id,
                      Mess_contenu AS Cont, 
                      Uti_pseudo AS Auteur
                      FROM messages  
                      join utilisateurs ON Uti_id = Mess_FK_Uti_id
                      WHERE mess_id =(Select Max(Mess_id)
                                      FROM messages  
                                      join utilisateurs on Uti_id = Mess_FK_Uti_id)');
    $Trucsdivers2 = $r->fetchAll();
    $r = $db->query('SELECT COUNT(*) as inscris from utilisateurs');
    $inscris = $r->fetchALL();
    $r = $db->query('SELECT COUNT(Suj_id) as nbsuj from sujets');
    $nsuj = $r->fetchALL();
    echo "<p>
            Nombre de sujets ouverts :
              <span class='nbSuj'>".$nsuj[0]  ['nbsuj']."
              </span>
          </p>
          <p>
            Nombre total de message(s) :
              <span class='nombreMessage'>".$Trucsdivers[0]['nbtot']."
              </span>
          </p>
          <p>
            Dernier message le :<span class='date'>";
              if (isset($Trucsdivers[0]['dernier']))
              {
              echo $Trucsdivers[0]['dernier'];
              }
              else
              {
                echo "Rien à afficher";
              }
             echo "</span>
          </p>
          <p>
            L'auteur du message est :
              <span class='auteur'>";
               if (!empty( $Trucsdivers2[0]['Auteur']))
                  {
                    if(isset($_SESSION['pseudo']) && $Trucsdivers2[0]['Auteur'] == $_SESSION['pseudo'])
                      {
                        echo "Vous";
                      }
                      else
                      {
                        echo $Trucsdivers2[0]['Auteur'];
                      }
                  }
                  else
                  {
                    echo "Rien à afficher";
                  }
        echo "</span>
          </p>
          <p>
            le contenu du message est :
              <span class='contenu'>";
                if (!empty( $Trucsdivers2[0]['Cont']))
                  {
                    $chaine = $Trucsdivers2[0]['Cont'];
                    $nbcar = strlen($chaine);
                      if ($nbcar > 55)
                        {
                          $car = substr($chaine,0,55);
                          echo $car."...";  
                        }
                        else
                        {
                          echo $Trucsdivers2[0]['Cont'];
                        }                         
                  }
                  else
                  {
                    echo "Rien à afficher";
                  };
        echo "</span>
          </p>
          <p>
            Il y a : 
              <span style='color:red; font-size:45px;'>(".$inscris[0]['inscris'].") 
              </span>membre(s) inscrit(s)
          </p>";

          
    ?>
  </div>