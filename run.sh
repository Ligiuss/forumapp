#!/bin/bash
lf="logs"

if [ -n "$1" ]
then
    if [ "$1" = "-n" ]
    then
        npm install >> $lf/npm.log
    fi
fi
mongod --dbpath db >> $lf/mongod.log &
P1=$!
sleep 5
node index.js >> $lf/nodejs.log &
P2=$!

wait $P1 $P2
echo ' ' >> $lf/mongod.log
echo ' ' >> $lf/nodejs.log
echo "App closed"
